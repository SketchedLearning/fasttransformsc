CC = gcc
CFLAGS = -O3 -march=native -std=c99 -pedantic -Wall -Wextra -Wshadow -Wpointer-arith -Wcast-qual -Wstrict-prototypes -Wmissing-prototypes -fopt-info-vec-optimized

build:
	git clone https://github.com/FALCONN-LIB/FFHT
	cd ./FFHT; \
	$(CC) fht.c -o fht.o -c $(CFLAGS) -fPIC; \
	$(CC) fast_copy.c -o fast_copy.o -c $(CFLAGS) -fPIC
	$(CC) fast_transforms.c -o fast_transforms.o -c $(CFLAGS) -fPIC -lm
	$(CC) -shared fast_transforms.o FFHT/fht.o FFHT/fast_copy.o -o ./libfasttransforms.so $(CFLAGS) -lm
