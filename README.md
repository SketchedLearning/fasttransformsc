# FastTransformsC

A very small C wrapper around the Falconn fast Walsh-Hadamard library. 
It provides:
- a function `fwht_double_batch` which will apply the Falconn fast Walsh-Hadamard transform on each column of a double matrix.
- two functions `triplespin_double_batch` and `triplespin_transpose_double_batch` to multiply by a "triplespin" operator (a product of diagonal and Walsh-Hadamard matrices) and its transpose.

This repository is used to generate the FastHadamardStructuredTransforms_jll package. The `build_tarballs.jl` file can be found on Yggdrasil.
