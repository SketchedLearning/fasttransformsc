#include <stdio.h>
#include <math.h>
#include "FFHT/fht.h"

/* Runs the falconn FFHT on each of the `b` blocks of size 2^(log_n) of each of the `cols` columns of `X`.  */
int fht_double_batch(double *restrict X, int log_n, int b, int cols);
int fht_double_batch(double *restrict X, int log_n, int b, int cols){
	int d = pow(2, log_n);
	int m = d*b;
	for(int i=0; i<cols; i++){
		int mi = m*i;
		for(int j=0; j<b; j++){  
			int cbl = mi+j*d; 
			fht_double(&X[cbl], log_n);
		}
	}
	return 0;
}

/* Used to multiply a "triplespin" operator (combination of fast Walsh-Hadamard matrices and diagonal matrices).  */
int triplespin_double_batch(double *restrict out, double *restrict X, double *restrict D, double *restrict R, int log_n, int b, int cols);
int triplespin_double_batch(double *restrict out, double *restrict X, double *restrict D, double *restrict R, int log_n, int b, int cols){
	int d = pow(2, log_n);
	int m = d*b;
	for(int i=0; i<cols; i++){ // columns
		int ci = i*d; int mi = m*i;
		for(int j=0; j<b; j++){  
			int jd = j*d; int cbl = mi+jd; int k; 
			for(k=0; k<d; k++) out[cbl+k] = X[ci+k]*D[2*m+jd+k]; 
			fht_double(&out[cbl], log_n); 
			for(k=0; k<d; k++) out[cbl+k] = out[cbl+k]*D[m+jd+k]; 
			fht_double(&out[cbl], log_n); 
			for(k=0; k<d; k++) out[cbl+k] = out[cbl+k]*D[jd+k]; 
			fht_double(&out[cbl], log_n);
			for(k=0; k<d; k++) out[cbl+k] = out[cbl+k]*R[jd+k];
		}
	}
	return 0;
}

// Transpose of a triplespin
int triplespin_transpose_double_batch(double *restrict out, double *restrict X, double *restrict D, double *restrict R, int log_n, int b, int cols);
int triplespin_transpose_double_batch(double *restrict out, double *restrict X, double *restrict D, double *restrict R, int log_n, int b, int cols){
	int d = pow(2, log_n);
	int m = d*b;
	int tm = 2*m;
	double x[d];
	for(int i=0; i<cols; i++){ 
		int ci = i*d; int mi = m*i; int k; 
		for(int j=0; j<b; j++){ 
			int jd = j*d; int cbl = mi+jd; 
			for(k=0; k<d; k++) x[k] = X[cbl+k]*R[jd+k]; 
			fht_double(&x[0], log_n); 
			for(k=0; k<d; k++) x[k] = x[k]*D[jd+k]; 
			fht_double(&x[0], log_n); 
			for(k=0; k<d; k++) x[k] = x[k]*D[m+jd+k]; 
			fht_double(&x[0], log_n); 
			if(j==0) 
				for(k=0; k<d; k++) out[ci+k] = x[k]*D[tm+jd+k]; 
			else 
				for(k=0; k<d; k++) out[ci+k] = out[ci+k] + x[k]*D[tm+jd+k];
		} 
		for(k=0; k<d; k++) out[ci+k] = out[ci+k];
	}
	return 0;
}
